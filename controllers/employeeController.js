        
const express = require('express');
var router = express.Router({ mergeParams: true });
const mongoose = require('mongoose');
const Employee = mongoose.model('Employee');
const nodemailer = require("nodemailer");
const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);
// Fork workers.
for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  // Workers can share any TCP connection
  // In this case it is an HTTP server
  http.createServer((req, res) => {
    res.writeHead(200);
    res.end('hello world\n');
  }).listen(8000);

  console.log(`Worker ${process.pid} started`);
}


router.get('/',(req,res) => {
   
    res.render('employee/addOrEdit',{
        viewTitle : 'Insert Employee',
        csrfToken : req.csrfToken()
    });
});
router.post('/',(req,res) => {
    if(req.body._id == ''){
        insertRecord(req,res);
    } else {
        updateRecord(req,res);
    }
})

function insertRecord(req,res){
    var employee = new Employee();
    employee.fullName = req.body.fullName;
    employee.email = req.body.email;
    employee.mobile = req.body.mobile;
    employee.save((err,doc) => {
        if(!err){
            //res.redirect('employee/list');
            let testAccount =  nodemailer.createTestAccount();

            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
              host: "smtp.ethereal.email",
              port: 465,
              secure: false, // true for 465, false for other ports
              auth: {
                user: testAccount.user, // generated ethereal user
                pass: testAccount.pass, // generated ethereal password
              },
            });

            // send mail with defined transport object
            let info =  transporter.sendMail({
              from: '"Fred Foo " <riddhirathod090@gmail.com>', // sender address
              to: "riddhi@designs.codes", // list of receivers
              subject: "Hello ✔", // Subject line
              text: "Hello world?", // plain text body
              html: "<b>Hello world?</b>", // html body
            });
          
            console.log("Message sent: %s", info.messageId);
            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

            // Preview only available when sending through an Ethereal account
            console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

            res.send(true);
        } else {
            res.send(err);
            // console.log("else");
            // console.log(err.name);
            // if(err.name == 'ValidationError'){
            //     handleValidationError(err,req.body);
            //     console.log(req.body);
            //     res.render('employee/addOrEdit',{
            //         viewTitle : 'Insert Employee',
            //         employee: req.body
            //     });
            // } else {
            //     console.log('Error generated during adding records : ' + err);
            // }
        }
    });
}
function updateRecord(req,res){
    Employee.findOneAndUpdate({_id: req.body._id},req.body,{new: true},(err,doc) => {
        if(!err){
            res.redirect('/employee/list');
        } else {
            if(err.name = 'ValidationError'){
                handleValidationError(err,req.body);
                res.render('employee/addOrEdit',{
                    viewTitle : 'Update Employee', 
                    employee: req.body
                });
            } else {
                console.log("Error while updating :"+ err);
            }
        }
    });
}       
function handleValidationError(err,body){
    for(field in err.erros){
        switch(err.errors[field].path){
            case 'fullName':
                body['fullNameError'] = err.errors[field].message;
                break;
            case 'email':
                body['emailError'] = err.errors[field].message;
                break;
            default:
                break;
        }
    }
}
router.get('/list',(req,res) => {
    Employee.find().lean().exec((err,docs) => {
        if(!err){
            // res.render('employee/list',{
            //     list: docs
            // });
            //res.send(docs);
            return res.sendFile(__dirname ,"../", '/views/output.html');
        } else {
            console.log("Error generated while listing :"+ err);
        }
    })
});

router.get('/view/:id',(req,res) => {
    Employee.findById(req.params.id,(err,doc) => {

        if(!err){
            res.send(doc);
        } else {
            res.send("error");
        }
    });
});
router.get('/:id',(req,res) => {
    Employee.findById(req.params.id,(err,doc) => {
        console.log(doc);
        console.log("inside edit");
        if(!err){
            const context = {
                
                    fullName: doc.fullName,
                    email: doc.email,
                    id: doc.id,
                    mobile: doc.mobile
                  
              }
              console.log(context);
            res.render('employee/addOrEdit',{
                viewTitle : 'Update Employee', 
                employee: context
            });
        } else {
            console.log("error");
        }
    });
   
});

router.get('/delete/:id',(req,res) => {
    Employee.findByIdAndRemove(req.params.id,(err,doc) => {
        
        if(!err){
            res.redirect('/employee/list');
        } else {
            console.log("Error while deleting :"+ err);
        }
    });
});
module.exports = router;