//https://www.youtube.com/watch?v=voDummz1gO0&t=471s
require('./models/db');

const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
var vhost = require('vhost');
const EventEmitter = require('events');

const path = require('path');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const csurf = require('csurf');
const cookieParser = require('cookie-parser');
//const io = io.listen(server);

const employeeController = require('./controllers/employeeController');

var app = express();

// Event call
const emitter = new EventEmitter();
emitter.on('messageLogged',function(arg){
   console.log("Listener called!",arg.id,arg.url);
})
emitter.emit('messageLogged',{'id':1,'url':'http://localhost'});


const csrfMiddleware = csurf({
   cookie: true
 });
 
// configure app
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({type: 'application/json', limit: '50mb'}));
app.use(cookieParser());
app.use(csrfMiddleware);

app.use(cors());


 var httpServer = require('follow-redirects').http; //require('http');
// http.createServer(function (request, response) {
//    response.writeHead(200, {'Content-Type': 'text/plain'});
//    response.end('Hello World! Node.js is working correctly.\n');
//}).listen(3005,"127.0.0.1");
//app.use(vhost('admin.mydomain.local', app2)); // Serves second app

const server = httpServer.createServer(app);
// app.use(bodyparser.urlencoded({
//   extended:true
// }));
// app.use(bodyparser.json());

app.set('views',path.join(__dirname,'/views/'));
app.engine('hbs',exphbs({extname: 'hbs', defaultLayout: 'mainLayout', layoutsDir: __dirname + '/views/layouts/'  }));
app.set('view engine','hbs');


server.listen(3002, () => {
   console.log('Express server started on port:3002');
});

// http.createServer(function (req, res) {
//   res.writeHead(200, {'Content-Type': 'text/plain'});
//   //res.end('Hello World\n');
// }).listen(3003, "127.0.0.1");
// http.createServer(app).listen(app.get('port'),
//   function(){
//     console.log("Express server listening on port " + app.get('port'));
// });

app.use('/employee',csrfMiddleware,employeeController);


module.exports = app;
// server.listen(3004, () => {
//   console.log('Express server started on port:3004');
// });

/*******************************From New Tutorial************************ */
//https://www.mongodb.com/blog/post/quick-start-nodejs-mongodb--how-to-get-connected-to-your-database
// const {MongoClient} = require('mongodb');
// console.log("Welcome");
// async function main() {
//    const uri = "mongodb+srv://riddhi:Riddhi@99@cluster0.wj6v3.mongodb.net/test?retryWrites=true&w=majority";
//    const client = new MongoClient(uri,{ useNewUrlParser: true, useUnifiedTopology: true });
//   //  await client.connect();
//   //  await listDatabases(client);
//    try {
//     await client.connect();
//     console.log('database connected');

//     await listDatabases(client);
 
//   } catch (e) {
//       console.error(e);
//   }
//   finally {
//     await client.close();
//   }
 

// }
// main().catch(console.error);

// async function listDatabases(client){
//   databasesList = await client.db().admin().listDatabases();

//   console.log("Databases:");
//   databasesList.databases.forEach(db => console.log(` - ${db.name}`));
// };

/*****************************From Old Tutorial************************************ */

// console.log('May Node be with you');

// const express = require('express');
// const bodyParser= require('body-parser');

// const app = express();

// app.use(bodyParser.urlencoded({ extended: true }))

// app.listen(3000, function() {
//     console.log('listening on 3000')
//   })

// const MongoClient = require('mongodb').MongoClient;
// const quotesCollection = "";
// const uri = "mongodb+srv://riddhi:Riddhi@99@cluster0.wj6v3.mongodb.net/test?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
// client.connect(err => {
//   const db_collection = client.db("test");
//   console.log("connected to DB");
//   const quotesCollection = db_collection.collection('quotes')

//  // client.close();
// });

//   app.get('/', function(req, res) {
//     //res.send('Hello World')
//     res.sendFile(__dirname + '/index.html');
//   })

//   app.post('/quotes', (req, res) => {
//     console.log(req.body);
//     console.log("inside create");
//     quotesCollection.insertOne(req.body)
//     .then(result => {
//       console.log(result)
//     })
//     .catch(error => console.error(error))
//   })

//   // MongoClient.connect('mongodb-connection-string', (err, client) => {
//   //   // ... do something here
//   // })
 
//   // MongoClient.connect(connectionString, {
//   //   useUnifiedTopology: true
//   // }, (err, client) => {
//   //   if (err) return console.error(err)
//   //   console.log('Connected to Database')
//   // })
//    // var url = 'mongodb://localhost/EmployeeDB';
//   // MongoClient.connect(url,function(err,db){
//   //   console.log("connected");
//   //   db.close();
//   // });
